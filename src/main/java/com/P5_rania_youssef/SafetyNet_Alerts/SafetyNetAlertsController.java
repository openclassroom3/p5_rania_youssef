package com.P5_rania_youssef.SafetyNet_Alerts;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
    public class SafetyNetAlertsController {

        @GetMapping("/")
        public String index() {
            return "Greetings from Spring Boot!";
        }

    }

